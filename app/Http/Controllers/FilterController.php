<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('timetracking');
    }

    /**
     * Get timesheet data and do some manipulation to show in the chart
     * @param Request $request
     */
    public function getTimeSheetData( Request $request ) {

        $filter_query = [];
        $current_time = Carbon::now();
        $now = Carbon::now();
        if( ! empty( $request->filter ) && $request->filter == '7d' ) {

            $filter_query['fromDate'] = $current_time->subDay(7)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else if( ! empty( $request->filter ) && $request->filter == '1m' ) {

            $filter_query['fromDate'] = $current_time->subMonth(1)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else if( ! empty( $request->filter ) && $request->filter == '1q' ) {

            $filter_query['fromDate'] = $current_time->subMonth(3)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else if( ! empty( $request->filter ) && $request->filter == '1y' ) {

            $filter_query['fromDate'] = $current_time->subMonth(12)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else {

            $filter_query['fromDate'] = $current_time->subDay(7)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        }
        $filter_query['limit'] = 9999;

        $response = Http::withHeaders([
            'User'      => 'f9c52717-13e0-4976-bf2f-992ae81648b9',
            'Key'       => 'd0c89e58a8e57301d6cce06cb6b02d3cd5f842e87e59ddfb64eb9c764b4a0163',
            'Secret'    => '891bd027fa94627ccb4c313687d2901a9a0597722e8c8fa912394ec0046945db'
        ])->get('https://publicapi.visualcare.com.au/timesheets', $filter_query);

        if( $response->successful() && $response->status() == 200 ) {
            $entries = json_decode($response->body());
            $minutes = 0;
            foreach( $entries as $entry  ) {
                $minutes += $entry->Minutes;
            }

            $totalbilledhours = 0;
            foreach( $entries as $entry  ) {
                if( isset( $entry->ChargeRate ) && $entry->ChargeRate == 0 ) {
                    $totalbilledhours += $entry->Minutes;
                }
            }
        }

        return json_encode(["success" => true, "totalworkedhours" => ($minutes/60), "totalbilledhours" => ($minutes - $totalbilledhours)/60 ]);
    }

    /**
     * Get timesheet data and do some manipulation to show in the chart
     * @param Request $request
     */
    public function getRoster( Request $request ) {
        $filter_query = [];
        $current_time = Carbon::now();
        $now = Carbon::now();
        if( ! empty( $request->filter ) && $request->filter == '7d' ) {

            $filter_query['fromDate'] = $current_time->subDay(7)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else if( ! empty( $request->filter ) && $request->filter == '1m' ) {

            $filter_query['fromDate'] = $current_time->subMonth(1)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else if( ! empty( $request->filter ) && $request->filter == '1q' ) {

            $filter_query['fromDate'] = $current_time->subMonth(3)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else if( ! empty( $request->filter ) && $request->filter == '1y' ) {

            $filter_query['fromDate'] = $current_time->subMonth(12)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        } else {

            $filter_query['fromDate'] = $current_time->subDay(7)->format('Y-m-d');
            $filter_query['toDate'] = $now->format('Y-m-d');

        }
        $filter_query['limit'] = 9999;

        $response = Http::withHeaders([
            'User'      => 'f9c52717-13e0-4976-bf2f-992ae81648b9',
            'Key'       => 'd0c89e58a8e57301d6cce06cb6b02d3cd5f842e87e59ddfb64eb9c764b4a0163',
            'Secret'    => '891bd027fa94627ccb4c313687d2901a9a0597722e8c8fa912394ec0046945db'
        ])->get('https://publicapi.visualcare.com.au/rosters', $filter_query);

        if( $response->successful() && $response->status() == 200 ) {
            $entries = json_decode($response->body());
            $minutes = 0;
            foreach( $entries as $entry  ) {
                $minutes += $entry->Minutes;
            }

            $totalbilledhours = 0;
            foreach( $entries as $entry  ) {
                if( isset( $entry->ChargeRate ) && $entry->ChargeRate == 0 ) {
                    $totalbilledhours += $entry->Minutes;
                }
            }
        }

        return json_encode(["success" => true, "totalworkedhours" => ($minutes/60), "totalbilledhours" => ($minutes - $totalbilledhours)/60 ]);
    }
}
