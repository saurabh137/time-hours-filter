<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Returning singe view.
Route::get('/', [App\Http\Controllers\FilterController::class, 'index'] );
Route::post('/timesheet', [App\Http\Controllers\FilterController::class, 'getTimeSheetData'] );
Route::post('/roster', [App\Http\Controllers\FilterController::class, 'getRoster'] );
