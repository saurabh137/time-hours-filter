@extends('layouts.app')

@section('contents')
    <div class="px-3.5 relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        @if (Route::has('login'))
            <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                @auth
                    <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
                @else
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}"
                            class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                    @endif
                @endauth
            </div>
        @endif
        <div class="w-full max-w-[1100px]">
            <div class="row flex flex-wrap -mx-5">
                <div class="w-full md:w-1/2 px-5 mb-5 md:mb-0">
                    <h3 class="text-3xl text-black font-bold mb-10 text-center">TimeSheet Data</h3>
                    <div class="bg-black/[.8] fixed w-full h-full left-0 top-0 right-0 bottom-0 flex justify-center items-center loader"><div class=""><img src="{{ ('/images/loader-img.gif') }}"></div></div>
                    <div class="flex item-center flex-wrap justify-between filter mb-8">
                        <label class="text-base text-black font-bold">Time Filter</label>
                        <select name="date-filter"
                            class="w-full md:w-1/4 date-filter focus:border-lbrwnclr outline-0 border border-neutral-400 h-8 px-2.5 rounded bg-neutral-100 w-full form-control">
                            <option value="">Select an option</option>
                            <option selected value="7d">Last 7 Days</option>
                            <option value="1m">Last Month</option>
                            <option value="1q">Last Quarter</option>
                            <option value="1y">Last Year</option>
                        </select>
                    </div>
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
                <div class="w-full md:w-1/2 px-5">
                    <h3 class="text-3xl text-black font-bold mb-10 text-center">Roster Data</h3>
                    <div class="flex item-center flex-wrap justify-between filter mb-8">
                        <label class="text-base text-black font-bold">Time Filter</label>
                        <select name="roster-date-filter"
                            class="w-full md:w-1/4 roster-date-filter focus:border-lbrwnclr outline-0 border border-neutral-400 h-8 px-2.5 rounded bg-neutral-100 w-full form-control">
                            <option value="">Select an option</option>
                            <option selected value="7d">Last 7 Days</option>
                            <option value="1m">Last Month</option>
                            <option value="1q">Last Quarter</option>
                            <option value="1y">Last Year</option>
                        </select>
                    </div>
                    <canvas id="roster-myChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- Adding chart & jQuery js using CDN(Use NPM) --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.0.0-alpha.1/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.8.0/dist/chart.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-date-fns/dist/chartjs-adapter-date-fns.bundle.min.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript">
        var data2 = {
            totalworkedhours: 0,
            totalbilledhours: 0
        }
        const ctx = document.getElementById('myChart').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Total Work Hours', 'Total Billed Hours'],
                datasets: [{
                    label: '# of Votes',
                    data: [data2.totalworkedhours, data2.totalbilledhours],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
        timesheet("7d");

        jQuery(document).on("change", ".date-filter", function() {
            timesheet(jQuery(this).val());
        });

        //------------------------------------------------//
        var data3 = {
            totalworkedhours: 0,
            totalbilledhours: 0
        }
        const rosterctx = document.getElementById('roster-myChart').getContext('2d');
        const rostermyChart = new Chart(rosterctx, {
            type: 'doughnut',
            data: {
                labels: ['Total Work Hours', 'Total Billed Hours'],
                datasets: [{
                    label: '# of Votes',
                    data: [data3.totalworkedhours, data3.totalbilledhours],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
        roster("7d");

        jQuery(document).on("change", ".roster-date-filter", function() {
            roster(jQuery(this).val());
        });

        function roster(filter) {
            loader(true);

            axios.post("{{ url('/roster') }}", {
                filter: filter
            }).then((response) => {

                data2 = {
                    totalworkedhours: response.data.totalworkedhours,
                    totalbilledhours: response.data.totalbilledhours
                }

                rostermyChart.data.datasets[0].data = [response.data.totalworkedhours, response.data.totalbilledhours];

                rostermyChart.update();

                loader(false);

            }).catch((error) => {
                loader(false);
                alert(error);
            });
        }

        function timesheet(filter) {
            loader(true);

            axios.post("{{ url('/timesheet') }}", {
                filter: filter
            }).then((response) => {

                data2 = {
                    totalworkedhours: response.data.totalworkedhours,
                    totalbilledhours: response.data.totalbilledhours
                }

                myChart.data.datasets[0].data = [response.data.totalworkedhours, response.data.totalbilledhours];

                myChart.update();

                loader(false);

            }).catch((error) => {
                loader(false);
                alert(error);
            });
        }

        function secondsToDhms(seconds) {
            seconds = Number(seconds);
            var d = Math.floor(seconds / (3600*24));
            var h = Math.floor(seconds % (3600*24) / 3600);
            var m = Math.floor(seconds % 3600 / 60);
            var s = Math.floor(seconds % 60);

            var dDisplay = d > 0 ? d + (d == 1 ? " day, " : " days, ") : "";
            var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
            var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
            var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
            return dDisplay + hDisplay + mDisplay + sDisplay;
        }

        function loader( loader ) {
            if( loader ) {
                jQuery(".loader").show();
            } else {
                jQuery(".loader").hide();
            }
        }
    </script>
@endsection
